package maps;

import core.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class DeleteCostumerMap {

    @FindBy(css = "input[placeholder='Search CustomerName']")
    public WebElement inpSearchName;
    public By tdName(String name){
        return By.xpath("//tbody/tr/td[3][contains(text(),'"+name+"')]");
    }
    @FindBy(css = ".select-row")
    public WebElement cboxName;
    @FindBy(css = ".delete-selected-button")
    public WebElement btnDelete;
    @FindBy(css = ".alert-delete-multiple-one")
    public WebElement textConfirmDelete;
    @FindBy(css = ".delete-multiple-confirmation-button")
    public WebElement btnDeleteConfirm;
    @FindBy(xpath = "/html/body/div[4]/span[3]/p")
    public WebElement textMsgSucessDelete;
}
