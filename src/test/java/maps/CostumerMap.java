package maps;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CostumerMap {
    @FindBy(id = "switch-version-select")
    public WebElement slcTheme;
    @FindBy(xpath = "//*[@id='gcrud-search-form']/div[1]/div/a")
    public WebElement linkAddRecord;
    @FindBy(css = "#field-customerName")
    public WebElement inpCustomerName;
    @FindBy(css = "#field-contactLastName")
    public WebElement inpLastName;
    @FindBy(css = "#field-contactFirstName")
    public WebElement inpFirstName;
    @FindBy(css = "#field-phone")
    public WebElement inpPhone;
    @FindBy(css = "#field-addressLine1")
    public WebElement inpAddressLine1;
    @FindBy(css = "#field-addressLine2")
    public WebElement inpAddressLine2;
    @FindBy(css = "#field-city")
    public WebElement inpCity;
    @FindBy(css = "#field-state")
    public WebElement inpState;
    @FindBy(css = "#field-postalCode")
    public WebElement inpPostalCode;
    @FindBy(css = "#field-country")
    public WebElement inpCountry;
    @FindBy(css = "#field-salesRepEmployeeNumber")
    public WebElement inpEmployerNumber;
    @FindBy(css = "#field-creditLimit")
    public WebElement inpCreditLimit;
    @FindBy(css = "#field-deleted")
    public WebElement inpDeleted;
    @FindBy(css = "#form-button-save")
    public WebElement btnSave;
    @FindBy(css = "#report-success > p")
    public WebElement textMsgSuccess;
    @FindBy(css = "#save-and-go-back-button")
    public WebElement btnGoBack;



}
