package steps.frontEnd;

import core.Driver;
import enums.Browser;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import org.junit.Assert;
import page.CostumerPage;

import java.io.IOException;
import java.util.Map;

public class CostumerSteps {
    CostumerPage costumerPage;
    Scenario cenario;
    @Before
    public void iniciaNavegador(Scenario _cenario){
        cenario = _cenario;
    }

    @Dado("que esteja na pagina {string}")
    public void queEstejaNaPagina(String url) {
        new Driver(Browser.CHROME);
        Driver.setNomeCenario(cenario.getName());
        Driver.criaDiretorio();

        Driver.getDriver().get(url);
        costumerPage = new CostumerPage();

    }
    @Quando("modifico a versao do bootstrap para {string}")
    public void modificoAVersaoDoBootstrapPara(String theme) {
        costumerPage.selectTheme(theme);
    }
    @Quando("adiciono um customer")
    public void adicionoUmCustomer() throws IOException {
        Driver.printScreen("Tema Alterado");
        costumerPage.clickAddRecord();
    }
    @Quando("preencho os campos com")
    public void preenchoOsCamposCom(Map<String, String> map) throws IOException {

        costumerPage.setCustomerName(map.get("name"))
                .setLastName(map.get("lastName"))
                .setFirstName(map.get("name"))
                .setPhone(map.get("phone"))
                .setAddressLine1(map.get("address1"))
                .setAddressLine2(map.get("address2"))
                .setCity(map.get("city"))
                .setState(map.get("state"))
                .setPostalCode(map.get("postalCode"))
                .setCountry(map.get("country"))
                .setEmployerNumber(map.get("employeer"))
                .setCreditLimit(map.get("creditLimit"));
        Driver.printScreen("Campos de cadatro preenchidos");
    }
    @Entao("clico no botao salvar e valido que a mensagem {string} foi exibida")
    public void clicoNoBotaoSalvarEValidoQueAMensagemFoiExibida(String msg) throws IOException {
        costumerPage.clickSave();
        Assert.assertEquals(msg, costumerPage.getMsgSuccess());
        Driver.printScreen("Mensagem de cadastro validada");
    }
}
