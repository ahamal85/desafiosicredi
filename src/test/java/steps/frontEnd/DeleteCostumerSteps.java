package steps.frontEnd;

import core.Driver;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import org.junit.Assert;
import page.CostumerPage;
import page.DeleteCostumerPage;

import java.io.IOException;

public class DeleteCostumerSteps {
    DeleteCostumerPage deleteCostumerPage;
    CostumerPage costumerPage;

    @Dado("que retorno para pagina principal")
    public void queRetornoParaPaginaPrincipal() {
        deleteCostumerPage = new DeleteCostumerPage();
        costumerPage = new CostumerPage();
        costumerPage.clickGoBack();
    }
    @Quando("pesquiso pelo nome {string} e seleciono o checkbox correspondente")
    public void pesquisoPeloNomeESelecionoOCheckboxCorrespondente(String nome) throws IOException {
        deleteCostumerPage.setSearchName(nome)
                .clickcboxName();
        Driver.printScreen("Pesquisa realizada");
    }
    @Quando("clico no botao delete")
    public void clicoNoBotaoDelete() {
        deleteCostumerPage.clickDelete();
    }
    @Entao("valido a mensagem {string}")
    public void validoAMensagem(String msg) throws IOException {
        Assert.assertEquals(msg, deleteCostumerPage.getConfirmDelete());
        Driver.printScreen("Validada mensagem de confirmacao");
    }
    @Quando("clico no botao delete da modal")
    public void clicoNoBotaoDeleteDaModal() {
        deleteCostumerPage.clickDeleteConfirm();
    }
    @Entao("valido a mensagem de confirmacao {string}")
    public void validoAMensagemDeConfirmacao(String msg) throws IOException {
        Assert.assertEquals(msg, deleteCostumerPage.getMsgSucessDelete());
        Driver.printScreen("Validada mensagem de exclusão");

        Driver.getDriver().quit();
    }
}
