package page;

import core.Driver;
import maps.CostumerMap;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class CostumerPage {
    CostumerMap costumerMap;
    public CostumerPage(){
        costumerMap = new CostumerMap();
        PageFactory.initElements(Driver.getDriver(), costumerMap);
    }

    public CostumerPage selectTheme(String theme){
        Select select = new Select(costumerMap.slcTheme);
        select.selectByVisibleText(theme);
        return this;
    }
    public CostumerPage clickAddRecord(){
        costumerMap.linkAddRecord.click();
        return this;
    }
    public CostumerPage setCustomerName(String customerName){
        costumerMap.inpCustomerName.sendKeys(customerName);
        return this;
    }
    public CostumerPage setLastName(String lastName){
        costumerMap.inpLastName.sendKeys(lastName);
        return this;
    }
    public CostumerPage setFirstName(String firstName){
        costumerMap.inpFirstName.sendKeys(firstName);
        return this;
    }
    public CostumerPage setPhone(String phone){
        costumerMap.inpPhone.sendKeys(phone);
        return this;
    }
    public CostumerPage setAddressLine1(String addressLine1){
        costumerMap.inpAddressLine1.sendKeys(addressLine1);
        return this;
    }
    public CostumerPage setAddressLine2(String addressLine2){
        costumerMap.inpAddressLine2.sendKeys(addressLine2);
        return this;
    }
    public CostumerPage setCity(String city){
        costumerMap.inpCity.sendKeys(city);
        return this;
    }
    public CostumerPage setState(String state){
        costumerMap.inpState.sendKeys(state);
        return this;
    }
    public CostumerPage setPostalCode(String postalCode){
        costumerMap.inpPostalCode.sendKeys(postalCode);
        return this;
    }
    public CostumerPage setCountry(String country){
        costumerMap.inpCountry.sendKeys(country);
        return this;
    }
    public CostumerPage setEmployerNumber(String employerNumber){
        costumerMap.inpEmployerNumber.sendKeys(employerNumber);
        return this;
    }
    public CostumerPage setCreditLimit(String creditLimit){
        costumerMap.inpCreditLimit.sendKeys(creditLimit);
        return this;
    }
    public CostumerPage setDeleted(String deleted){
        costumerMap.inpDeleted.sendKeys(deleted);
        return this;
    }
    public CostumerPage clickSave(){
        costumerMap.btnSave.click();
        return this;
    }

    public String getMsgSuccess(){
        Driver.visibilityOf(costumerMap.textMsgSuccess);
        return costumerMap.textMsgSuccess.getText();
    }

    public CostumerPage clickGoBack(){
        costumerMap.btnGoBack.click();
        return this;
    }
}