package page;

import core.Driver;
import maps.DeleteCostumerMap;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.PageFactory;

public class DeleteCostumerPage {
    DeleteCostumerMap deleteCostumerMap;
    public DeleteCostumerPage(){
        deleteCostumerMap = new DeleteCostumerMap();
        PageFactory.initElements(Driver.getDriver(), deleteCostumerMap);
    }

    public DeleteCostumerPage setSearchName(String searchName){
        Driver.visibilityOf(deleteCostumerMap.inpSearchName);
        deleteCostumerMap.inpSearchName.sendKeys(searchName);
        deleteCostumerMap.inpSearchName.sendKeys(Keys.ENTER);
        Driver.visibilityOf(deleteCostumerMap.tdName(searchName));
        return this;
    }


    public DeleteCostumerPage clickcboxName(){
        Driver.visibilityOf(deleteCostumerMap.cboxName);
        deleteCostumerMap.cboxName.click();
        return this;
    }
    public DeleteCostumerPage clickDelete(){
        deleteCostumerMap.btnDelete.click();
        return this;
    }
    public String getConfirmDelete(){
        Driver.visibilityOf(deleteCostumerMap.textConfirmDelete);
        return deleteCostumerMap.textConfirmDelete.getText();
    }
    public DeleteCostumerPage clickDeleteConfirm(){
        deleteCostumerMap.btnDeleteConfirm.click();
        return this;
    }
    public String getMsgSucessDelete(){
        Driver.visibilityOf(deleteCostumerMap.textMsgSucessDelete);
        return deleteCostumerMap.textMsgSucessDelete.getText();
    }
}
