#language:pt
@Customer
Funcionalidade: Customer

  Cenario: Cadastro customer
    Dado que esteja na pagina "https://www.grocerycrud.com/v1.x/demo/my_boss_is_in_a_hurry/bootstrap"
    Quando modifico a versao do bootstrap para "Bootstrap V4 Theme"
    E adiciono um customer
    E preencho os campos com
      | name        | Teste Sicredi         |
      | lastName    | Teste                 |
      | contact     | Adriana Hamal         |
      | phone       | 51 9999-9999          |
      | address1    | Av Assis Brasil, 3970 |
      | address2    | Torre D              |
      | city        | Porto Alegre          |
      | state       | RS                    |
      | postalCode  | 91000-000             |
      | country     | Brasil                |
      | employeer   | 123456                |
      | creditLimit |                   200 |
    Entao clico no botao salvar e valido que a mensagem "Your data has been successfully stored into the database. Edit Record or Go back to list" foi exibida
    Dado que retorno para pagina principal
    Quando pesquiso pelo nome "Teste Sicredi" e seleciono o checkbox correspondente
    E clico no botao delete
    Entao valido a mensagem "Are you sure that you want to delete this 1 item?"
    Quando clico no botao delete da modal
    Entao valido a mensagem de confirmacao "Your data has been successfully deleted from the database."

